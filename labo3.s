// Raphael Valois (valr2802) - Etienne Boutet (boue2327)

.global main

.macro SAVE
	stp 	x29,x30, [sp, -96]!
	mov 	x29, sp
	stp 	x27,x28, [sp,16]
	stp		x25,x26, [sp,32]
	stp		x23,x24, [sp,48]
	stp		x21,x22, [sp,64]
	stp		x19,x20, [sp,80]
.endm

.macro RESTORE
	ldp		x27,x28, [sp,16]
	ldp		x25,x26, [sp,32]
	ldp		x23,x24, [sp,48]
	ldp		x21,x22, [sp,64]
	ldp		x19,x20, [sp,80]
	ldp		x29,x30, [sp],96
.endm

// Programme qui:
//  - lit un entier 1 ≤ n ≤ 1024 et des entiers signés a1, a2, ..., an
//  - affiche [an, ..., a2, a1]

main:

	// Lire les valeurs de tab
	adr		x0, tab
	bl		read_tab			// read_tab(tab)
	mov		x19, x0

	// Inverser la tab
	adr		x0, tab
	mov		x1, x19
	bl		rev_tab				// rev_tab(tab, n)

	// Afficher le tableau
	adr		x0, tab
	mov		x1,	x19
	bl		print_tab			// print_tab(tab, n)
	mov		x0,	0
	bl		exit

// Sous-programme: rev_tab
// Entrées:
//   - adresse a
// Effet: — lit un entiernnon signé de 64-bits (formatfmtLen) où 1 <= n <= 1024
//		  — lit n entiers signés de 64-bits (formatfmtElem)
//		  — stocke ces n entiers, consécutivement, à partir de l’adresse a
// Sortie: n
read_tab:						// int n read_tab(a)
	SAVE						// 	{
	mov		x20, x0				//		tab = a	
	mov		x19, 0				//		i = 0
	adr		x0, fmtLen			//	
	adr		x1, nombre			//
	bl		scanf				//		scanf(&fmtLen, nombre)
	ldr		x21, nombre			//		n = &nombre
readElem:						//		do {
	adr		x0, fmtElem			//
	mov		x1, x20				//		
	bl		scanf				//			scanf(&fmtElem, tab)
	add		x19, x19, 1			//			i++
	add		x20, x20, 8			//			tab = tab + 8
	cmp		x19, x21			//		}
	b.lo	readElem			//		while(i < n)
	mov		x0, x21				//		return n
	RESTORE						//
	ret							//	}

// Sous-programme: rev_tab
// Entrées:
//   - adresse d’un tableau t d’entiers signés de 64-bits
//	 - nombre d’éléments du tableau t
// Effet: renverse l’ordre des éléments du tableauten mémoire
// Sortie: Aucune
rev_tab:						// void rev_tab(t[], n)
	SAVE						// {
	sub		x1, x1, 1			//    n = n - 1
	mov		x19, 8				//    temp = 8
	mul		x19, x1, x19		//    temp = n * temp
	mov		x20, x0				//    pointeur = *t[] 
	mov		x21, x0				//    pointeur2 = *t[]
	add		x21, x21, x19		//    pointeur2 = pointeur2 + temp
loop:							//    loop
	cmp		x20, x21			//    if(pointeur >= pointeur2)
	b.hs	endloop				//    break
	ldr		x22, [x20]			//    nombre1 = &pointeur
	ldr		x23, [x21]			//    nombre2 = &pointeur2
	str		x22, [x21]			//    *pointeur2 = nombre1
	str		x23, [x20]			//    *pointeur = nombre2
	add		x20, x20, 8   		//    pointeur = pointeur + 8
	sub		x21, x21, 8			//	  pointeur2 = pointeur2 - 8
endloop:						//
	RESTORE						//
	ret							// }

// Sous-programme: print_tab
// Entrées:
//   - adresse d'un tableau tab
//   - taille n de tab
// Effet:  affiche les éléments de tab
// Sortie: aucune
print_tab:// void print_tab(tab[], n)
	SAVE// {
	mov		x19, x0				//
	mov		x20, x1				//   i = n
	adr		x0, fmtDebut		//
	bl		printf				//   afficher "["

print_tab100:					//
	adr		x0, fmtElem			//   do {
	ldr		x1, [x19],8			//		v = tab[n-i]
	bl		printf				//		afficher v
	sub		x20, x20, 1			//		i--
	cmp		x20, 0				//		if (i == 0)
	b.eq	print_tab200		//			break
	adr		x0, fmtSep			//		else
	bl		printf				//			afficher ", "
	b		print_tab100		//   }

print_tab200:					//
	adr		x0, fmtFin			//   afficher "]\n"
	bl		printf				//
	RESTORE						//
	ret							// }

.section	".rodata"
fmtLen:		.asciz	"%lu"
fmtElem:	.asciz	"%ld"
fmtDebut:	.asciz	"["
fmtSep:		.asciz	", "
fmtFin:		.asciz	"]\n"

.section	".bss"
			.align	8
tab:		.skip	1024*8// Alloue 1 Kio de mémoire
			.align  8
nombre:		.skip 	8
